(function(angular, undefined) {
'use strict';

angular.module('budgettrApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);
'use strict';

angular.module('budgettrApp.auth', ['budgettrApp.constants', 'budgettrApp.util', 'ngCookies',
    'ui.router'
  ])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
